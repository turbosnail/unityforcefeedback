#ifndef __MAIN_H__
#define __MAIN_H__

#define STRICT
#define DIRECTINPUT_VERSION 0x0800

#include <windows.h>
#include <dinput.h>
#include <math.h>

/*  To use this exported function of dll, include this header
 *  in your project.
 */


#define EXPORT_API __declspec(dllexport)


//-----------------------------------------------------------------------------
// Function prototypes
//-----------------------------------------------------------------------------
BOOL CALLBACK EnumFFDevicesCallback( const DIDEVICEINSTANCE* pInst, VOID* pContext );
BOOL CALLBACK EnumAxesCallback( const DIDEVICEOBJECTINSTANCE* pdidoi, VOID* pContext );

bool bFinedDevice = false;

#ifdef __cplusplus
extern "C"
{
#endif

EXPORT_API HRESULT InitDirectInput(HWND hDlg );
EXPORT_API HRESULT Aquire();
EXPORT_API HRESULT SetDeviceForcesXY(INT x, INT y);
EXPORT_API HRESULT StartEffect();
EXPORT_API HRESULT StopEffect();
EXPORT_API HRESULT SetAutoCenter(bool autoCentre);
EXPORT_API VOID FreeDirectInput();

#ifdef __cplusplus
}
#endif

//-----------------------------------------------------------------------------
// Defines, constants, and global variables
//-----------------------------------------------------------------------------
#define SAFE_DELETE(p)  { if(p) { delete (p);     (p)=NULL; } }
#define SAFE_RELEASE(p) { if(p) { (p)->Release(); (p)=NULL; } }

#define FEEDBACK_WINDOW_X       20
#define FEEDBACK_WINDOW_Y       60
#define FEEDBACK_WINDOW_WIDTH   200

LPDIRECTINPUT8          g_pDI = NULL;
LPDIRECTINPUTDEVICE8    g_pDevice = NULL;
LPDIRECTINPUTEFFECT     g_pEffect = NULL;
BOOL                    g_bActive = TRUE;
DWORD                   g_dwNumForceFeedbackAxis = 0;
DWORD                   g_dwLastEffectSet; // Time of the previous force feedback effect set

#endif // __MAIN_H__
